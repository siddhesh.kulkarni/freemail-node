const axios = require('axios');

/**
 * @typedef {{
 * successLabel: string;
 * successHelperText: string;
 * returnURL: string;
 * clientName: string;
 * successSubjectText: string;
 * }} SuccessTemplateData
 */

/**
 * @typedef {{
 * logText: string;
 * clientName: string;
 * logTime: string;
 * }} LogsTemplateData
 */

/**
 * @typedef {{
 * apiKey: string;
 * url?: string;
 * }} FreeMailerAttributes
 */

class FreeMailer {
  /**
   * Singleton static email service instance
   * @type {FreeMailer}
   * @static
   */
  static instance = null;

  /**
   * Creates a singleton class  
   * Use `FreeMailer.register({})` instead of `new FreeMailer()`
   * @private
   * @param {FreeMailerAttributes} attr 
   */
  constructor(attr) {
    if (!FreeMailer.instance) {
      FreeMailer.instance = this;
    } else {
      throw new Error('FreeMailer instance already available, please use `FreeMailer.instance`');
    }
    this.apiKey = "";
    this.url = "https://freemaileronline.herokuapp.com";

    Object.assign(this, attr);
  }

  /**
   * Creates a singleton class.
   * @example
   * FreeMailer.register({
   *   apiKey: 'ldTnffK03rrx4JckY6p45KFA8l3vk5sj'
   * });
   * @param {FreeMailerAttributes} attr 
   * @returns {FreeMailer} Singleton instance of FreeMailer
   */
  static register(attr) {
    return new FreeMailer(attr);
  }

  /**
   * Creates a post call to send success type of email.
   * Uses default email success template.
   * @example
   * FreeMailer.instance.sendSuccess('myemail@server.com', {
   *   successLabel: 'Success!!',
   *   successHelperText: 'Your request has been approved',
   *   returnURL: 'https//www.cmsone.site/'
   *   clientName: 'CMS One'
   *   successSubjectText: 'Your request has been approved - CMS One'
   * });
   * @param {string | string[]} to Email recipients
   * @param {SuccessTemplateData} templateData Template data
   * @returns {Promise<any>} Promise for post call
   */
  sendSuccess(to, templateData) {
    const data = {
      apiKey: this.apiKey,
      to,
      template: "SUCCESS",
      templateData
    };

    return axios.post(this.url + "/send", data);
  }

  /**
   * Creates a post call to send success type of email.
   * Uses default email success template.
   * @example
   * FreeMailer.instance.sendSuccess('myemail@server.com', {
   *   logText: "log 1 <br/> log 2",
   *   clientName: "CMS One",
   *   logTime: "05-06-2021 16:57"
   * });
   * @param {string | string[]} to Email recipients
   * @param {LogsTemplateData} templateData Template data
   * @returns {Promise<any>} Promise for post call
   */
  sendLogs(to, templateData) {
    const data = {
      apiKey: this.apiKey,
      to,
      template: "LOGS",
      templateData
    };

    return axios.post(this.url + "/send", data);
  }

  /**
   * Composes and sends email
   * @example
   * FreeMailer.instance.send('myemail@server.com', 'Welcome to CMS One', 'Welcome! <br> Sign up to continue');
   * @param {string | string[]} to Email recipient(s)
   * @param {string} subject Subject for the email.
   * @param {string} html HTML string, please use inline css.
   * @returns {Promise<any>} Promise for post call
   */
  send(to, subject, html) {
    const data = {
      apiKey: this.apiKey,
      to, subject, html
    };

    return axios.post(this.url + "/send", data);
  }
}

module.exports = { FreeMailer };
