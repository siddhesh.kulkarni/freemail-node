# freemail-node

> Driver for Freemailer Online API

API is live at https://freemaileronline.herokuapp.com

## Usage
### Import
```
const { FreeMailer } = require('freemail-node');
```
### Register
```
FreeMailer.register({
  apiKey: 'ldTnffK03rrx4JckY6p45KFA8l3vk5sj'
});
```
### Send!
```
FreeMailer.instance.send('myemail@server.com', 'Welcome to CMS One', 'Welcome! <br> Sign up to continue');
```
### Use an existing template
```
FreeMailer.instance.sendSuccess('myemail@server.com', {
  successLabel: 'Success!!',
  successHelperText: 'Your request has been approved',
  returnURL: 'https//www.cmsone.site/'
  clientName: 'CMS One'
  successSubjectText: 'Your request has been approved - CMS One'
});
```

## Get Trial API Key
> To get an API Key, send email with you company/organization details at `freemaileronline@gmail.com`.  
Details should contain, Company full name, public email address and private email address.